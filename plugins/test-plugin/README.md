# Plugin: `test-plugin`

- **status**: permanently unstable
- **type**: whatever is needed

This is a plugin used for testing things out. Should never be used in production.
