# Contributing

The LibResilient project welcomes contributions.

Best way to start is to deploy a testing deployment (for example using the `python3 -m http.server` method) and start testing things out to understand the code.

Bug reports are welcome as new issues. Code contributions are welcome as merge requests.

The project does not require a [CLA](https://en.wikipedia.org/wiki/Contributor_License_Agreement) of any sort. All contributed code will be published under the AGPLv3 license, along with the rest of the project.

## AI-generated code

**The LibResilient project does not accept any kind of AI-generated code**.

By contributing code to LibResilient you acknowledge the above and certify that the code you are contributing has not been generated in any part with help of any AI-based code generation tool (for example, GitHub Copilot).

Contributions that are based on AI-generated code will be rejected outright, and Contributors who submit them will be banned from the project.
