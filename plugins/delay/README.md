# Plugin: `delay`

- **status**: beta
- **type**: wrapping

This plugin wraps a plugin, and delays returning the response from it by configurable amount.

## Configuration:

TBD


