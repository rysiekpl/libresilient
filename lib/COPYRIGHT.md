# Copyright

Copyrights and licenses for files in this directory are as following:

## JS-IPFS Project

The following files are part of the [js-ipfs](https://github.com/ipfs/js-ipfs) project, and are licensed under the MIT license:
 - `ipfs.js`

## GunDB Project

The following files are part of the [GunDB](https://github.com/amark/gun) project, and are licensed under the Apache license:
 - `gun.js`
 - `sea.js`
 - `webrtc.js`
 
## `file-type` project

The following files are generated based on the [`file-type`](https://github.com/sindresorhus/file-type/) project. The project itself is licensed under MIT license. The files below include bundled dependencies of the `file-type` project, all of which are either licensed under MIT or BSD 2-Clause licenses:
 - `file-type.js`
